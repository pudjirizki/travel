<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous"/>
    <link rel="stylesheet" href="style.css" />
    <!-- google font size -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;700&display=swap"
      rel="stylesheet"
    />
    <!-- logo title bar -->
    <link rel="Icon" href="./assets/Logo Icon.png" type="image/x-icon" />
  </head>
  <body>
    <!-- NAVBAR -->
    <nav
      class="navbar navbar-expand-lg navbar-dark bg-primary position-fixed w-100">
      <div class="container-fluid">
        <a class="navbar-brand" href="#">
          <img  src="{{asset('asset/LogoIcon.png')}}" alt="" class="d-inline-block align-text-top me-3"/>
          Rumah Impian
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav mx-auto">
            <li class="nav-item mx-auto">
              <a class="nav-link active" aria-current="page" href="#">Home</a>
            </li>

            <li class="nav-item mx-auto">
              <a class="nav-link" href="#">Paket Tour</a>
            </li>
            <li class="nav-item mx-auto">
              <a class="nav-link" href="#">Sewa Mobil</a>
            </li>
            <li class="nav-item mx-auto">
              <a class="nav-link" href="#">Sewa Boat</a>
            </li>
            <li class="nav-item mx-auto">
              <a class="nav-link" href="#">Penginapan</a>
            </li>
            <li class="nav-item mx-auto">
              <a class="nav-link" href="#">Blog</a>
            </li>
            <li class="nav-item mx-auto">
              <a class="nav-link" href="#">KONTAK</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- AKIR NAVBAR -->
    <!-- hero section -->
    <section id="hero">
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-md-6 hero-tagline my-auto">
            <h4>SULTAN D`RAJA TOUR.</h4>
            <p>
              Kami Menyediakan Solusi Wisata yang Tepat Sesuai Keinginan Anda.
            </p>
            <button class="btn btn-primary">Pesan Sekarang</button>
          </div>
        </div>
        <img src="{{asset('asset/LogoIcon.png') }}"alt=""class="position-absolute end-0 bottom-0"/>
      </div>
    </section>
  <!-- AKHIR HERO  -->

  <!-- Layanan -->
    <section id="layanan">
      <div class="container ">
        <div class="row my-5">
          <div class="col-md-12">
            <h4 class="fw-semibold">Pilih Paket Tour Populer</h4>
          </div>
        </div>
      </div>
      <section>
        <div class="container d-flex justify-content-center">
          <div class="col-md-4">
            <div class="card">
              <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
              <div class="price-tag">500.000/Pax</div>
              <div class="card-body">
                <p class="card-text fw-bold">Tour Nusa Panila Bali</p>
              </div>
              <ul class="list-group list-group-flush">
                <li class="list-group-item">
                  <img src="./assets/clock (1).png" alt=""/> 1 Day Tour
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card">
              <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
              <div class="price-tag">500.000/Pax</div>
              <div class="card-body">
                <p class="card-text fw-bold">Tour Nusa Panila Bali</p>
              </div>
              <ul class="list-group list-group-flush">
                <li class="list-group-item">
                  <img src="./assets/clock (1).png" alt=""/> 1 Day Tour
                </li>
              </ul>
            </div>
          </div>
            <div class="col-md-4">
              <div class="card">
                <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
                <div class="price-tag">500.000/Pax</div>
                <div class="card-body">
                  <p class="card-text fw-bold">Tour Nusa Panila Bali</p>
                </div>
                <ul class="list-group list-group-flush">
                  <li class="list-group-item">
                    <img src="./assets/clock (1).png" alt=""/> 1 Day Tour
                  </li>
                </ul>
              </div>
            </div>
       </section>
    </section>

    <section>
      <div class="container d-flex justify-content-center">
        <div class="col-md-4">
          <div class="card">
            <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
            <div class="price-tag">500.000/Pax</div>
            <div class="card-body">
              <p class="card-text fw-bold">Tour Nusa Panila Bali</p>
            </div>
            <ul class="list-group list-group-flush">
              <li class="list-group-item">
                <img src="./assets/clock (1).png" alt=""/> 1 Day Tour
              </li>
            </ul>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card">
            <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
            <div class="price-tag">500.000/Pax</div>
            <div class="card-body">
              <p class="card-text fw-bold">Tour Nusa Panila Bali</p>
            </div>
            <ul class="list-group list-group-flush">
              <li class="list-group-item">
                <img src="./assets/clock (1).png" alt=""/> 1 Day Tour
              </li>
            </ul>
          </div>
        </div>
          <div class="col-md-4">
            <div class="card">
              <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
              <div class="price-tag">500.000/Pax</div>
              <div class="card-body">
                <p class="card-text fw-bold">Tour Nusa Panila Bali</p>
              </div>
              <ul class="list-group list-group-flush">
                <li class="list-group-item">
                  <img src="./assets/clock (1).png" alt=""/> 1 Day Tour
                </li>
              </ul>
            </div>
            <div class="col-md-12 d-flex justify-content-end mt-4">
              <button class="btn btn-primary">Lihat Semua</button>
            </div>
          </div>
     </section>

     <section>
      <div class="container justify-content-center">
        <div class="row my-5">
          <div class="col-md-12">
            <h4 class="fw-semibold">Daerah Wisata</h4>
            <!-- <div class="container d-flex justify-content-center"> -->
            <div class="row">
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
            </div>
            <!-- <div class="container d-flex justify-content-center"> -->
            <div class="row">
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
                </div>
                <div class="col-md-12 d-flex justify-content-end mt-4">
                  <button class="btn btn-primary">Lihat Semua</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
     <section>
      <div class="container justify-content-center">
        <div class="row my-5">
          <div class="col-md-12">
            <h4 class="fw-semibold">Mobil Dan Boat</h4>
            <!-- <div class="container d-flex justify-content-center"> -->
            <div class="row">
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
            </div>
            <!-- <div class="container d-flex justify-content-center"> -->
            <div class="row">
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <br>
    <section>
      <div class="container justify-content-center">
        <div class="row my-5">
          <div class="col-md-12">
            <h4 class="fw-semibold">Hotel Dan Penginapan</h4>
            <!-- <div class="container d-flex justify-content-center"> -->
            <div class="row">
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
            </div>
            <!-- <div class="container d-flex justify-content-center"> -->
            <div class="row">
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi5.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi2.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
                </div>
                <div class="col-md-12 d-flex justify-content-end mt-4">
                  <button class="btn btn-primary">Lihat Semua</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section>
      <div class="container justify-content-center">
        <div class="row my-5">
          <div class="col-md-12">
            <h4 class="fw-semibold">Mengapa Harus Memilih Kami ?</h4>
          </div>
        </div>
      </div>
      <div class="container justify-content-center">
        <div class="row my-5">
          <div class="col-md-12">
            <!-- <div class="container d-flex justify-content-center"> -->
            <div class="row">
              <div class="col-md-3">
                <div class="card-wisata">
                 <h6 class="fw-bold">Kualitas pelayanan Terbaik</h6>
                 <p>kerja keras serta pelayanan dengan sepenuh hati merupakan dedikasi kami untuk memberikan kualitas terbaik bagi anda </p>
                </div>
              </div>
              <div class="col-md-3">
                <div class="card-pilihan">
                  <img src="./assets/Fitur Rumah 1.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
              <div class="col-md-3">
                <div class="card-pilihan">
                  <img src="./assets/Fitur Rumah 1.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
              <div class="col-md-3">
                <div class="card-pilihan">
                  <h6 class="fw-bold">Bebas Dari Masalah</h6>
                 <p>melalui pelayanan yang menyeluruh dalam segala aspek, anda tidak perlu mengurus banyak hal dan menjadikan liburan anda seperti sebuah pekerjaan</p>
                </div>
              </div>
            </div>
          <div class="col-12">
            <!-- <div class="container d-flex justify-content-center"> -->
            <div class="row">
              <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                <div class="card-wisata">
                  <h6 class="fw-bold">Penyediaan Perjalanan Terbaik </h6>
                 <p>memori dan cerita perjalanan anda adalah prioritas utama bagi kami. kami memastikan keseleruhan proses liburan anda berjalan baik dan tak terlupakan</p>
                </div>
              </div>
              <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                <div class="card-pilihan">
                  <img src="./assets/Fitur Rumah 3.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
              <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                <div class="card-pilihan">
                  <img src="./assets/Fitur Rumah 3.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
              <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                <div class="card-pilihan">
                  <h6 class="fw-bold">Pelayanan Terbaik Dan Personal</h6>
                  <p>penyediaan layanan yang terbaik bagi setiap pribadi menjadi prinsip kami dalam menyempurnakan perjalanan anda.</p>
                </div>
              </div>
            </div>
          </div>
    </section>

    <section>
      <div class="container d-flex justify-content-center">
        <div class="row my-5">
          <div class="col-12">
            <h4 class="fw-semibold">Galery</h4>
            <div class="row">
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
            <!-- <div class="container d-flex justify-content-center"> -->
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
              <div class="col-md-4">
                <div class="card-wisata">
                  <img src="./assets/Rekomendasi1.png" class="card-img-top" alt="Tour Image" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- </div> -->
    </section>

    <section id="layanan">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h4 class="fw-semibold">Rivew Customer Kami</h4>
          </div>
        </div>
        <div class="row mt-5">
          <div class="col-md-4 text-center">
            <div class="card-layanan">
              <div class="circle-icon position-relative mx-auto">
                <img src="./assets/profile-user_64.png" alt="" class="">
              </div>
              <h3 class="mt-3 fw-semibold">Agus</h3>
              <h6>bogor</h6>
                <p class="mt-3 text-black">pelayanan nya memuaskan sekali crew ramah dan asik,perjalanan sangat hati hati</p>
            </div>
          </div>
          <div class="col-md-4 text-center">
            <div class="card-layanan">
              <div class="circle-icon position-relative mx-auto">
                <img src="./assets/profile-user_64.png" alt="" class="">
              </div>
              <h3 class="mt-3">Kurniwan</h3>
              <h6>jakarta</h6>
                <p class="mt-3 text-black">perjalan asik soalnya para crewnya ramah,selalu hati hati, penginapanya bersih</p>
            </div>
          </div>
          <div class="col-md-4 text-center">
            <div class="card-layanan">
              <div class="circle-icon position-relative mx-auto">
                <img src="./assets/profile-user_64.png" alt="" class="">
              </div>
              <h3 class="mt-3">Ujang</h3>
              <h6>Bandung</h6>
                <p class="mt-3 text-black">Rrekomen bange menyewa tour disini karena pelayanannya matep banget fasilitasnya juga juara</p>
            </div>
          </div>
        </div>
      </div>
    </section>
<!-- FOOOTER -->

<footer>
  <div class="footer ">
    <div class="container d-flex ">
        <div class="col-12">
          <div class="row my-5">
            <div class="col-md-4">
              <div class="card-footer text-start">
              <h5 class="fw-semibold">Sultan D`Raja_Tour</h5>
              <h5>
                <img src="./assets/Logo Icon.png" alt="">
              </h5>
            </div>
          </div>
            <div class="col-md-4">
              <div class="card-footer text-start">
              <h6>
                <img src="./assets/Instagram Iicon.png" alt=""> Sultan D`raja_Tour
              </h6>
              <h6>
                <img src="./assets/Twiter Icon.png" alt="">  Sultan D`raja_Tour
              </h6>
              <h6>
                <img src="./assets/Whatsapp icon.png" alt=""> 081-123-435-789
              </h6>
            </div>
            </div>
            <div class="col-md-4">
              <div class="card-footer text-start" >
                <h6>Kebijakan Privasi</h6>
                  <h6> Profile Perusahaan </h6>
                <h6>Syarat Dan Kondisi</h6>
              </div>
            </div>
            </div>
            <hr>
          </div>
        </div>
    </div>
</footer>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  </body>
</html>

